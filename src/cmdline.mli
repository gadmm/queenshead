(** This module includes [Cmdliner.Term.t] definitions. They allow the user to
    modify the behaviour of the programs in some ways. Check out the manual page
    (or the output of --help) of the binary for user-oriented information. *)

(** The pattern is an argument allowing the user to chose the sort of name
    that the pub name generator outputs: "The NOUN of NOUN" or "The two
    QUALIFIER NOUNS" or whathaveyou. *)
val pattern : Pattern.pattern option Cmdliner.Term.t

(** The seed is an argument allowing the user to specify the PRNG seed. It is
    useful for determinism but not intended to be set for normal use. *)
val seed : int Cmdliner.Term.t

(** The count is an argument allowing the user to specify the number of pub
    names that the pub name generator generates. *)
val count : int Cmdliner.Term.t

(** The vars is an argument allowing the user to suppress pub name generation
    and instead display the list of variables usable in patterns. *)
val vars : bool Cmdliner.Term.t
