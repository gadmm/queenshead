(** Nouns:
    mono- (1), bi- (2), or poly- (more) syllabic (or any if unspecified)
    singular (s), or plural (p) (or either if unspecified) *)

(**)
val nouns1s : string Pick.choice
val nouns1p : string Pick.choice
val nouns2s : string Pick.choice
val nouns2p : string Pick.choice
val nounsmores : string Pick.choice
val nounsmorep : string Pick.choice
val nounss : string Pick.choice
val nounsp : string Pick.choice
val nouns : string Pick.choice
(**)

(** Qualifiers (like adjectives but English grammar is mismatched to latin
    concepts):
    mono- (1), bi- (2), or poly- (more) sylabic (or any if unspecified) *)

(**)
val qualifiers1 : string Pick.choice
val qualifiers2 : string Pick.choice
val qualifiers : string Pick.choice
(**)

(** Numbers:
    cardinals or ordinals *)

(**)
val cardinal : string Pick.choice
val ordinal : string Pick.choice
(**)

(** vars is a map for the variables usable in the patterns *)
val vars : string Pick.choice Astring.String.Map.t
