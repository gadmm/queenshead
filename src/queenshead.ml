let run_one (pattern : Pattern.pattern option) =
  let pattern =
    match pattern with
    | Some p -> p
    | None -> Pick.pick Pattern.default
  in
  Pattern.run pattern
;;

let drive seed pattern count vars =
  if vars
  then (
    if seed <> -1 || pattern <> None || count <> 1
    then failwith "The argument --pattern-vars may only be used by itself";
    Seq.iter
      (fun (v, _) -> Printf.printf "%s\n" v)
      (Astring.String.Map.to_seq Variables.vars))
  else (
    let () = if seed < 0 then Random.self_init () else Random.init seed in
    for _ = 1 to count do
      run_one pattern
    done)
;;

let main_v =
  let open Cmdliner in
  Cmd.(
    v
      (info ~doc:"British pub name generator." "queenshead")
      Term.(const drive $ Cmdline.seed $ Cmdline.pattern $ Cmdline.count $ Cmdline.vars))
;;

let main () =
  let code = Cmdliner.Cmd.eval main_v in
  exit code
;;

(* execute main generator *)
let () = main ()
