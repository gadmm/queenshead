open Astring

let data_of_filename filename =
  match Sys.getenv_opt filename with
  | None -> Option.get (Data.read filename)
  | Some fname ->
    let ic = open_in fname in
    let l = in_channel_length ic in
    let data = really_input_string ic l in
    close_in ic;
    data
;;

let nouns_of_file filename =
  let singulars, plurals =
    data_of_filename filename
    |> String.cuts ~sep:"\n"
    |> List.filter (String.exists (( = ) ','))
    |> List.map (fun s ->
      match String.cuts ~sep:"," s with
      | [ singular; "" ] -> singular, singular ^ "s"
      | [ singular; plural ] -> singular, plural
      | _ -> raise (Failure "Malformed noun"))
    |> List.split
  in
  Pick.arr (Array.of_list singulars), Pick.arr (Array.of_list plurals)
;;

(* for better results, give nouns in the bishbashbosh vowel order, split by
   approximately mono-, bi-, and more-syllabic. *)
let nouns1s, nouns1p = nouns_of_file "NOUNS1"
let nouns2s, nouns2p = nouns_of_file "NOUNS2"
let nounsmores, nounsmorep = nouns_of_file "NOUNSMORE"
let nounss = Pick.choices [| nouns1s; nouns2s; nounsmores |]
let nounsp = Pick.choices [| nouns1p; nouns2p; nounsmorep |]
let nouns = Pick.choices [| nounss; nounsp |]

let words_of_file filename =
  data_of_filename filename
  |> String.cuts ~sep:"\n"
  |> List.filter (fun s -> String.length s >= 1)
  |> Array.of_list
  |> Pick.arr
;;

let qualifiers1 = words_of_file "QUALIFIERS1"
let qualifiers2 = words_of_file "QUALIFIERS2"
let qualifiers = Pick.choices [| qualifiers1; qualifiers2 |]

let cardinal = words_of_file "CARDINAL"
let ordinal = words_of_file "ORDINAL"

let vars =
  String.Map.of_list
    [ "NOUN_MONO_SINGULAR", nouns1s
    ; "NOUN_MONO_PLURAL", nouns1p
    ; "NOUN_BI_SINGULAR", nouns2s
    ; "NOUN_BI_PLURAL", nouns2p
    ; "NOUN_MULTI_SINGULAR", nounsmores
    ; "NOUN_MULTI_PLURAL", nounsmorep
    ; "NOUN_SINGULAR", nounss
    ; "NOUN_PLURAL", nounsp
    ; "NOUN", nouns
    ; "QUALIFIER_MONO", qualifiers1
    ; "QUALIFIER_BI", qualifiers2
    ; "QUALIFIER", qualifiers
    ; "CARDINAL", cardinal
    ; "ORDINAL", ordinal
    ]
;;
