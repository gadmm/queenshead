(** A choice is a set of different discrete things to chose from. *)
type 'a choice

(** [arr a] is a [choice] for the elements of the array. *)
val arr : 'a Array.t -> 'a choice

(** [choices c] is a [choice] for the elements of the choices in [c]. Note that
    each element is as likely to be picked. E.g.,
    [choices (arr [|0|]) (arr [|1; 2|])] will produce (in average) as
    many [0]s as [1]s as [2]s. *)
val choices : 'a choice Array.t -> 'a choice

(** [pick c] picks an element of [c] *)
val pick : 'a choice -> 'a

(** [pick2 c] picks two elements of [c]. The order of the elements is preserved. *)
val pick2 : 'a choice -> 'a * 'a
