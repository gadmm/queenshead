let pattern =
  let open Cmdliner in
  let parser = function
    | "possessive" -> Ok Pattern.Possessive
    | "x-and-y" -> Ok Pattern.Anded
    | "qualified" -> Ok Pattern.qualified
    | "numbered" -> Ok Pattern.qualified
    | s ->
      (match Bos.Pat.of_string s with
       | Ok p -> Ok (Pattern.Free p)
       | Error (`Msg s) -> Error (`Msg ("Invalid pattern: " ^ s)))
  in
  let printer fmt = function
    | Pattern.Possessive -> Format.fprintf fmt "possessive"
    | Pattern.Anded -> Format.fprintf fmt "x-and-y"
    | Pattern.Free p -> Bos.Pat.pp fmt p
  in
  Arg.(
    value
      (opt
         (some (conv ~docv:"PATTERN" (parser, printer)))
         None
         (info
            ~absent:"a suitable pattern is chosen by the program"
            ~docv:"PATTERN"
            ~doc:
              "The pattern of pub name to generate. You can use one of the following \
               special patterns: `possessive` (e.g., 'The Queen's head'), `x-and-y` \
               (e.g., 'The Swan and duck'), `qualified` (e.g., 'The Copper Kettle'), or \
               `numbered` (e.g., 'The three cats'). You can also make a pattern \
               free-hand using the syntax `\\$(VAR)` to substitute in a VAR. You can use \
               any of the following for VAR: NOUN, QUALIFIER, CARDINAL, ORDINAL. E.g., \
               'The \\$(ORDINAL) \\$(QUALIFIER) \\$(NOUN)'."
            [ "pattern" ])))
;;

let seed =
  let open Cmdliner in
  Arg.(
    value
      (opt
         int
         (-1)
         (info
            ~absent:"non-deterministic"
            ~docv:"SEED"
            ~doc:
              "The seed to use for the pseudo-randomness of the generator. If none is \
               given, or if a negative number is given, the pseudo-randomness \
               self-initialises and the run is non-deterministic."
            [ "seed" ])))
;;

let count =
  let open Cmdliner in
  Arg.(
    value
      (opt int 1 (info ~docv:"COUNT" ~doc:"The number of names to generate." [ "count" ])))
;;

let vars =
  let open Cmdliner in
  Arg.(
    value
      (flag
         (info
            ~docv:"PATTERNVARS"
            ~doc:
              "Instead of generating a pub name: dump all the available pattern \
               variables. This is incompatible with any of the other parameters."
            [ "pattern-vars" ])))
;;
