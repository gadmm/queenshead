# The Queen's head (`queenshead`)

The Queen's head is a name generator for British pubs. It includes a single
binary executable which generates British pub names:

```
$ queenshead
The gold buck
```

## Installation and build instructions

You can install from opam

```
$ opam install queenshead
…
$ queenshead
The rose and yoke
```

Or you can compile from sources.

```
$ git clone https://gitlab.com/raphael-proust/queenshead.git
…
$ cd queenshead
…
$ opam install --deps-only .
…
$ dune build
…
$ ./_build/install/default/bin/queenshead
The Kings' aligator
```

## Usage

The `queenshead` executable takes the following optional parameters:

- `--help` to display full usage information
- `--count <number>` to generate multiple pub names
- `--seed <number>` to specify the pseudo-random number generator seed
- `--pattern <pattern>` to specify the pattern of names generated

Patterns can be either one of the keywords "possessive", "x-and-y", "qualified",
"numbered" or a free-hand pattern. A free-hand pattern is a string where some
variables are substituted for a random pick. The available variables can be
listed with the `--pattern-vars` parameter. E.g.,

```
$ queenshead --pattern "The \$(QUALIFIER) \$(QUALIFIER) \$(NOUN)"
The red wretched witches
```

(Beware of escaping in your terminal.)
